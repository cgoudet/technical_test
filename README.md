# technical_test

This repository contains multiple elements to help junior data scientists improve the skills needed in the industry.

* **questions_data_scientist** presents my answers to common questions asked by students.

* **skills_data_scientits** presents a set of skills that recruiters look for in a data scientist, and are too often neglected by formations.

* Each directory (corporate_finance, ...) represents a real technical test proposed in an interview for a data scientist position.
  Some of these directories also contain an answer that allowed the candidate to pass this interview step.
  A word of caution : those solutions were *enough* at that time.
  That does not mean they were perfect or even without any errors or limitations. 


## Ruunning notebooks

In order to run the notebooks, you need to install a virtual environment.
```
python -m virtualenv venv

#activave the virtualenv at every connection
source venv/bin/activate

#install the libraries
pip -r requirements.txt
```
