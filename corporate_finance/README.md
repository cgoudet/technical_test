# Corporate finance

## Context
You are a start-up company specialising in the sale of lift pods.
You are entering an already very competitive market (Loxam, Kiloutou, ...) where the reference players of the market are already very well identified.
Your business model is to offer online sale of lift pods with a promise of an agreement in principle within one minute of the request.
You are the only player to offer this kind of "full online" services, and your promise is therefore to give more agility to construction companies that have rapid needs of supply of pods.
On the other hand, since you are not very well known in the market and you do not have the means of communication, you decide to play on the price of your services by offering your product at a price of 10,000€ HT to companies, with a gross margin rate of 15%, rather low in the sector.
To simplify the use case, it is assumed that each request correspond to a single lift pod per company.

By having such low prices, you know you are going to have a lot of business applications that haven't been accepted by your competitors, probably because of an overly large credit risk.
Those rejected prospects are companies which have a high likelihood to not be able to pay for the lift pod.
So, knowing that you are going to have a lot of companies with high risk of default, you need to be extremely vigilant about pod applications that you validate if you want to earn money at the end of your fiscal year.
It is announced that 50% of the companies that are part of your customer potential will fail quickly after the pod request and will not pay for your service.
This type of situation should be avoided because even if they are rare, they can cause the death of your young company.
Indeed, one successful sale generate a 15% margin (1,500€).
However, one single default will generate a 8,500 loss, erasing the effect of 6 successful sales.

## Objective

As the data scientist, you have to propose a strategy to decide which prospects can be accepted.
You have several constraints to manage for this startup to be profitable.
* You have to accept at least 20% of the requests so that you don't get a reputation to refuse everyone.
* You must choose the prospects so that the total profit of the company is positive, and the highest possible.

You have at your disposition a database (train.csv) of 200 past prospects, for which you have all financial information available.
Each column correspond to an aggregate of their financial declaration.
The description of the columns is provided in the file "matching.csv".
The dataset also tags the companies which historically would not have been able to pay for the bill (column "default").

One you have defined your scoring strategy, you will be expected to present it to a round of investors that you will need to convince to invest in your startup.
You will need to show that you scoring strategy is in agreement with the financial expertise they possess.
Finally, you will be able to simulate the first year of your startup by applying the scoring strategy to the dataset of 800 prospects available in "test.csv", and check your profitability in the future.
You must also provide to the investor a solution to test and evaluate your model on single company.
