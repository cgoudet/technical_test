# Food startup

The technical test contains 4 themes to be answered in roughly one hour.
The quality of the answer is more important than the timing.
The candidate can take extra time if to finalise his/her answers.

The 4 themes are :
* SQL
* General programming
* Optimization
* Machine learning

## SQL

When we sell products we create transactions and store them in a table that looks like this:

| transaction_id | user_id | offer_type | amount | created_at          | company  | product_name    |
|----------------|---------|------------|--------|---------------------|----------|-----------------|
| 1002           | 43435   | Fridge     | 8.31   | 2020-01-02 10:15:00 | Bouygues | Tagliatelles    |
| 1003           | 43435   | Fridge     | 1      | 2020-01-02 11:15:00 | Bouygues | Coca-Cola       |
| 1004           | 43435   | On-demand  | 5.43   | 2020-01-03 12:14:00 | Bouygues | Penne légumes   |
| 1005           | 12411   | Fridge     | 4.13   | 2020-01-03 09:14:00 | Molitor  | Chili Con Carne |

We have two different offers: fridge and delivery.
offer_type refer to either of those and we use the term On-demand to talk about delivery

When a client purchases a product from Foodles, their employer can decide to grant them a subsidy.
Example:
- Client 43435 purchase a "Penne légumes" On Demand.
- The cost of Penne legumes is 8.73
- The employer decides to subsidies 3.3 for this item

=> We create a transaction record with amount 5.43 (8.73 - 3.3) that is payed by the employee
=> We create a subsidy record with amount 3.3 that is paid by the employer
=> The total turnover for Foodles on this transaction is : 5.43 + 3.3 = 8.73

We store those subventions in a table that looks like this:

| subvention_id | transaction_id | amount |
|---------------|----------------|--------|
| s123          | 1002           | 2.0    |
| s124          | 1002           | 1.2    |
| s125          | 1004           | 3.3    |
| s126          | 1005           | 2.1    |


We created a postgres instance containing mock data for you to try your queries,
It is available at :
postgres://qvrngwbcqhfjky:ee8ef57188354bb09766e700b5f5cd7d89a432ed2b8532709437758402d7ab64@ec2-52-212-157-46.eu-west-1.compute.amazonaws.com:5432/d783movlbhdcot

Otherwise you can find a dump of the data here:
https://foodles-tech-interview.s3.amazonaws.com/latest.dump

Once you downloaded it run:
> createdb test_foodles -O {your_postgres_user}
> pg_restore --verbose --clean --no-acl --no-owner -h localhost -U {your_postgres_user} -d test_foodles latest.dump

Please write SQL queries to answer the following questions, we use postgres and the database is a postgres
instance but you can choose the dialect of your liking

- How many transactions were entitled to a subsidy ?
- How many transactions were entitled to a subsidy per company ?

## General programming

### Write a function that takes as input (sentence: String, n: Number)

and returns a list of size `n` where each element contains a word and the frequency of that word in `sentence`
This list should be sorted by decreasing frequency.

Example:
```
Input:
("baz bar foo foo zblah zblah zblah baz toto bar", 3)

Output:
[
   ("zblah", 3),
   ("bar", 2),
   ("baz", 2),
]
```

## Optimization

Each day we need to dispatch dishes to fridges.
For instance if we have 500 pasta dishes, 200 salads for 20 fridge, how should we dispatch them ?

The data that we have are:
- Past consumption per dish per fridge
- Stock per dish remaining in each fridge

The constraint that we have are:
- Each fridge can not contain more than 20 dishes
- Some client impose that we dispatch at least n dishes in their fridge

Propose a general approach to solve this problem, you don't need to write code simply explain your thinking
and the tools you would need to solve this.

## Machine learning

We have a new menu each week, every dish from the menu is totally new.
Each week we need to order food from our supplier for this new menu.

We have at our disposal the table described in exercise 1 which describe
the consumption of past menus.

How would you estimate the quantities that we need to order for each dish on the new menu ?
- How would you create your dataset ?
- Which variables would you use ?
- Which KPI would you track ?
