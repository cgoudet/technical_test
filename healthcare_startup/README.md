# Healthcare startup

The dataset provided contains 13533 recording of patients visiting the healthcare center and 43 measurements.

* visit_id: Identify uniquely the patient's visit to the healthcare center (different from patient_id, not provided here)
* sex, age: Obvious
* feat_X: X [1,39]  represent either biomedical measurements (blood sample for instance), or information about the patient collected by clinician. It is not required for this exercise to know which is which or what they mean.

## Research
Quickly read article(s) and summarize your understanding of data imputation techniques.

## Analysis
Analyse the dataset provided. What can you say ? How will that affect your model ?

## Modeling ability
It has been proven using clinical testing that feat_27 is highly predictive to detect ear infection, yet not always measured.
	If feat_27 > 3: High risk
	elif feat_27 > 1.5: Mild risk
	else No risk
Build a model to predict ear infection when feat_27 is actually not measured.

Present the pros and cons of your model as well as any idea you have to improve your ear infection detection model.

## Thinking and Creativity
Think about another application using the dataset provided (and potentially other information) that could help anyone in the Healthcare Industry (patient, physician, hospitals, pharmaceuticals...). Draft the algorithm you would use to implement that idea.

## Communication
Summarize your answers in a presentation of ~25min.
