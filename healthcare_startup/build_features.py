from category_encoders.target_encoder import TargetEncoder
from sklearn.compose import ColumnTransformer
from category_encoders.cat_boost import CatBoostEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import PolynomialFeatures
from sklearn.cluster import DBSCAN
from sklearn.impute import KNNImputer
import numpy as np
import re
import itertools
import pandas as pd
import joblib
from pathlib import Path

def split(df, target):
    strat = StratifiedKFold(3, random_state=54, shuffle=True)
    train_ind, valid_ind = next(strat.split(df, df[target]))

    strat = StratifiedKFold(2, random_state=55, shuffle=True)

    tmp = df.iloc[valid_ind]
    valid_ind, test_ind = next(strat.split(tmp, tmp[target]))

    return df.index[train_ind], tmp.index[valid_ind], tmp.index[test_ind]


def check_missing(df, cols):
    missings = df[cols].isnull().mean(0)
    missings = missings[missings > 0]
    if len(missings):
        print(missings)
        raise RuntimeError('Missing values')


def add_interaction(df, cols, poly, train=False):
    check_missing(df, cols)
    if train:
        poly.fit(df[cols])
    extra_cols = ['_'.join(x) for x in itertools.combinations_with_replacement(cols, 2)]
    extra = pd.DataFrame(poly.transform(df[cols]),
                         columns=cols + extra_cols,
                         index=df.index)
    out = pd.concat([df, extra[extra_cols]], axis=1)
    return out


def filled_columns(df, limit=0.5):
    filled = df.select_dtypes(include=[np.number]).notnull().mean(axis=0)
    filled = filled[filled > limit]
    return sorted(filled.index)


def useless_col(x):
    parts = x.split('_')
    if ((len(parts) == 4)
        and (set(parts[[0, 2]]) == set(['isna', 'nona']))
            and (parts[1] == parts[3])):
        return True
    return False


def drop_useless(df):
    to_drop = ['sex', 'visit_id', 'age_age']
    # useless_poly = list(np.loadtxt(ROOTDIR / 'data' / 'interim' / 'useless_poly.txt', dtype=str, ndmin=1))
    # to_drop += list(filter(useless_col, df.columns))
    # to_drop = sorted(set(useless_cols + useless_poly) & set(df.columns))

    uniques = df.nunique()
    to_drop += list(uniques[uniques == 1].index)

    missings = df[[x for x in df.columns if x.startswith('feat')]].isnull().sum()
    missings = missings[missings > 0]
    to_drop += list(missings.index)
    to_drop = sorted(set(to_drop) & set(df.columns))
    return df.drop(columns=to_drop)


def name_from_groups(isna_group, nona_group):
    return 'isna_{}_nona_{}'.format(
        ''.join(str(x) for x in sorted(isna_group)),
        ''.join(str(x) for x in sorted(nona_group)))


def add_stat_groups(df, isna_group, nona_group):
    cols = ([f'isna_{n}' for n in isna_group]
            + [f'nona_{n}' for n in nona_group])
    tmp = df[cols]
    name = name_from_groups(isna_group, nona_group)
    df['group_sum_'+name] = tmp.sum(axis=1)
    df['group_prod'+name] = tmp.product(axis=1)


def add_groups_features(df):
    groups = [('age', [13, 18, 37, 39], [11]),
              ('age', [18, 37], [11]),
              ('feat_24', [0, 9], [5]),
              ('feat_31', [1, 28], [14]),
              ('feat_34', [1, 19, 21], []),
              ('feat_16', [29, 36, 21, 38], [14])
              ]

    for col, isna, nona in groups:
        add_stat_groups(df, isna, nona)
        add_interaction_with_group(df, col, isna, nona)


def add_interaction_with_group(df, col, isna_group, nona_group):
    # isna_groups = [13, 18, 37, 39]
    # nona_groups = [11]

    cols = ([f'isna_{n}' for n in isna_group]
            + [f'nona_{n}' for n in nona_group])
    tmp = df[cols]

    for i in range(2):
        step = 'or' if i else 'and'
        colname = '{}_{}_{}'.format(col, step, name_from_groups(isna_group, nona_group))
        df[colname] = df[col] * (tmp.max(axis=1) if i else tmp.product(axis=1))


def shift_features(df):
    pat = re.compile(r'^feat_[0-9]{1,2}$')
    to_shift = list(filter(lambda x: pat.match(x), df.columns)) + ['age']
    df[to_shift] = df[to_shift]+10


def add_poly_features(df):
    pat = re.compile(r'^feat_[0-9]{1,2}$')
    to_interact = list(filter(lambda x: pat.match(x), df.columns)) + ['age']
    poly = PolynomialFeatures(2, include_bias=False)
    df = add_interaction(df, to_interact, poly, train=True)
    # add_groups_features(df)

    return df


def add_imputation(df):
    imp_cols = sorted(set([x for x in filled_columns(df, limit=0.01) if x.startswith('feat')])-set(['feat_27']))
    imputer = KNNImputer(n_neighbors=10, weights='distance')
    out = imputer.fit_transform(df[imp_cols])

    imp_cols = [f'imp_{x}' for x in imp_cols]
    for c in imp_cols:
        df[c] = 0
    df[imp_cols] = out


def get_train_valid(df, target):
    train_cols = filled_columns(df, limit=0.01)
    train_cols = [x for x in train_cols if not x[-2:] == '27']

    trainable = df['feat_27'].notnull()

    df = df.copy()
    del df['feat_27']
    add_imputation(df)
    remove_full_empty(df)
    shift_features(df)
    df.fillna({k: -99 for k in train_cols}, inplace=True)
    df = add_poly_features(df)
    df = add_umap_isna(df)
    df['age_no_13'] = df['age'] * (df['feat_13'] == -99)

    df = drop_useless(df)

    train_ind, valid_ind, test_ind = split(df[trainable], target)
    cats = ['pipe_umap_isna_label']
    train = df.loc[train_ind].copy()
    train, encoder = add_cat_encodings(train, cats)
    valid = df.loc[valid_ind].copy()
    valid, _ = add_cat_encodings(valid, cats, encoder=encoder)

    return train, valid


def get_transformer_name(tf):
    return [f'{y}_{x[0]}' for x in tf.transformers_ for y in x[2] if x[0] != 'remainder']


def add_cat_encodings(df, cats, target='target_mild', encoder=None):
    if encoder is None:
        encoder = ColumnTransformer([('catboost', CatBoostEncoder(cols=cats), cats),
                                     ('targetenc', TargetEncoder(cols=cats), cats)
                                     ])
        encoder.fit(df, df[target])

    out = pd.DataFrame(encoder.transform(df), columns=get_transformer_name(encoder), index=df.index)
    df = pd.concat([df, out], axis=1)
    return df, encoder


def remove_full_empty(df):
    full_empty = df.isnull().mean(0)
    full_empty = list(full_empty[full_empty > 0.99].index)
    print('remove almost emtpy', len(full_empty))
    df.drop(columns=full_empty, inplace=True)


def add_umap_isna(df):

    encodings = [df]
    fn = 'pipe_umap_isna'

    enc = joblib.load(Path(fn).with_suffix('.joblib'))

    out = pd.DataFrame(enc.transform(df[enc.features_]), columns=[f'{fn}_{i}' for i in range(2)])
    encodings.append(out)

    knn = joblib.load(Path(fn.replace('pipe', 'knn')).with_suffix('.joblib'))
    labels = pd.DataFrame(knn.predict(out), columns=[fn+'_label'])
    encodings.append(labels)

    df = pd.concat(encodings, axis=1)
    return df
