from build_features import check_missing
from sklearn.metrics import mean_squared_error, roc_auc_score
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier


def test_rf(params, train, valid, target, train_cols):
    check_missing(train, train_cols)
    check_missing(valid, train_cols)
    if 'target' in target:
        rf = RandomForestClassifier(**params)
    else:
        rf = RandomForestRegressor(**params)
    rf.fit(train[train_cols], train[target])
    valid_preds = rf.predict(valid[train_cols])

    ax = None

    if target == 'target':
        loss_train = log_loss(train[target], rf.predict_proba(train[train_cols]))
        valid_proba = rf.predict_proba(valid[train_cols])
        loss_valid = log_loss(valid[target], valid_proba)

        #ax = confusion_matrix(valid[target], valid_preds)

    elif target == 'target_mild':
        loss_train = roc_auc_score(train[target], rf.predict_proba(train[train_cols])[:, 1])
        valid_proba = rf.predict_proba(valid[train_cols])[:, 1]
        loss_valid = roc_auc_score(valid[target], valid_proba)

    else:
        loss_train = mean_squared_error(train[target], rf.predict(train[train_cols]))
        loss_valid = mean_squared_error(valid[target], valid_preds)

        fig, ax = plt.subplots()
        ax.scatter(valid[target], valid_preds, color='cornflowerblue', alpha=0.1)
        ax.set_ylabel('preds')
        ax.set_xlabel('truth')

    return loss_train, loss_valid, ax, rf
