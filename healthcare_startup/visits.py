import numpy as np
import pandas as pd


def add_missing_tag(df):
    all_feat = [x for x in df.columns if x.startswith('feat')]
    no_tag = [15, 17, 26, 24, 16, 34]

    for col in all_feat:
        ind = int(col.split('_')[-1])
        if ind in no_tag:
            continue
        df[f'isna_{ind}'] = np.int8(df[col].isnull())
        df[f'nona_{ind}'] = np.int8(df[col].notnull())


def load():
    df = pd.read_csv('data_extract.csv')
    df['target'] = pd.cut(df.feat_27, [-6, 1.5, 3, 10], labels=['no_risk', 'mild', 'high'])
    df['target_mild'] = df.feat_27 > 1.5
    df['is_female'] = np.where(df.sex == 'F', 10, 1)

    df.drop(columns=['visit_id'], inplace=True)

    add_missing_tag(df)

    print(df.shape)
    return df
