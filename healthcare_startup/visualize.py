import matplotlib.pyplot as plt
import numpy as np


def draw_hist(df, col, title='', **kwargs):
    fig, ax = plt.subplots()
    ax.hist(df[col], **kwargs)
    if title:
        ax.set_title(title, loc='left')
    ax.set_xlabel(col)
    return ax


def compare_hists(df, col, masks, n_bins=20):
    desc = df[col].describe()
    bins = np.linspace(desc['min'], desc['max'], n_bins+1)
    opts = dict(bins=bins, histtype='step', density=True)

    fig, ax = plt.subplots()

    for label, mask in masks:
        ax.hist(df.loc[mask, col], label=label, **opts)

    ax.set_xlabel(col)
    fig.legend()
    return ax


def compare_scatter(df, colx, coly, xlim=None):
    tmp = df[[colx, coly]].dropna()

    fig, ax = plt.subplots()
    ax.scatter(tmp[colx], tmp[coly], color='cornflowerblue', alpha=0.2)

    ax.set_ylabel(coly)
    ax.set_xlabel(colx)
    if xlim:
        ax.set_xlim(*xlim)
    return ax
