# Questions on the data scientist job

I have been asked several times questions about the daily activities of data scientist and what is required to get a job in the field.
This file combines to most frequent questions and my point of view at the time.

### What are your daily task as a Data scientist?

I have multiple types of tasks in my work.
This set of tasks also depends on the company.
Some companies are more mature and have specialized roles related to data, while some others really need a data scientist who is more polyvalent.
Here are the full tasks I have been given so far.

*	**Understanding business problem**
Before starting to define a ML solution, the data scientist needs to think about what is the real problem to solve.
He/She needs to talk with domain experts and identify the differences between what they need and what they think they need.
The real problem may not even require machine learning.
About 50% of data projects fail because the data scientist goes straight to the modelisation of the problem he/she wish he/she had and, even though the model works, it does not solve the real business problem.

*	**Data exploration**
I spend a lot of time doing simple data analysis do understand my data, what does it contain and how it was generated.
This step also helps to get more knowledge about the domain.
I can identify patterns that I don’t understand and discuss with domain expert to understand their meaning, and understand how I can deal with this pattern in my solution.

* **Solution design**
The solution to the problem is never a model alone.
The model must interact with an environment to provide value.
Then, the data scientist must design the whole architecture of the solution: when and which data are available, when does the IA model should and should not be used, how the output will be integrated into the interface…

*	**Modelisation**
The modelisation step is the action of trying different strategies to create a predictive model.
This is what is discussed the most.

*	**Explanation**
Usually ML solutions are used in a critical part of the business.
Errors can have a major impact. Then experts want to know on which ground an algorithm takes a specific decision. As a baseline, the model should reproduce the basic expert knowledge of the field. The data scientist should then study how the model behave under different situation so that he/she can present this logic to the experts. The data scientist should also look for biases in the model and mitigate them.

*	**Software development**
The data scientist spends a lot of time developing code. This could be to create the algorithm and to train it, but also for data processing or deployment. This requires the data scientist to be proficient in software development, and to follow best practices for readability and code efficiency. It is even more required if the project requires collaborative work with other developers/data scientists.

* **Deployment**
A ML solution has value only if it is actually used by its target users. This means that the users must be able to reach it and interact with it. There is a whole field of data which is specific to how to put the model in production. There are many strategies depending on the environment in which the solution will live in. In larger companies there is a specific job for that: ML engineer. In less mature companies the data scientist is expected to understand enough software and deployment engineering to deploy its solution. Furthermore, he/she will be expected to spend time maintaining the solution (debugging, update libraries, improve technical performances, improve model performances, …)


### Is a data scientist specialized in a field (big data, economy, health…)? Or it is a person who could be assigned to different mission regardless the domain?
A data science project requires to have expert knowledge to ensure that meaningful data are included in a meaningful way in the solution. This expert knowledge can be brought by domain experts. However, it is important for the data scientist to understand the domain to identify bad data and bad modelling. This knowledge can be acquired during the project so I don’t think the data scientist should already be a domain expert before starting the project. However, the more expert is the data scientist, the more likely is the success of the project.

As an example, I started my career analyzing data in subatomic physics. I then went to a startup working for patients in French hospitals. I had no real knowledge of how were hospitals organized at that time. After that, I went to a company that predict whether a French company will go bankrupt. At this point, again, I had no prior knowledge of corporate finance (even though it was central in the project) but I learned the most basic concepts on the spot.

### For you, what are the needed hard and soft skills of a data scientist?
In my view a data scientist should have a deep understanding of the inner working of the techniques he/she uses. This knowledge will be essential to understand whether the data are adapted to a specific algorithm, and how the algorithm will be able to leverage the information in the data. Too often I see (junior) data scientist applying a recipe without realizing that it is suboptimal or even totally wrong with respect to the situation.

A data scientist should have problem solving skills and mindset. A major source of failing projects is the application of recipes, where one problem requires a certain type of algorithms. Usually the situation is subtler and the data scientist should aim to define properly the problem and understand how various techniques may be able to solve it.
A data scientist should also be curious to always try to understand what patterns the algorithm has found, in order to better understand the predictions, and identify problems. The more understanding the data scientist has about the model, the less likely will issues happen.

A data scientist should have communication skills to present results to non-experts and also be able to explain the various data science concepts in plain words. This is because he/she will be required to discuss those with domain experts that may not be data scientists. This will help create synergy within the project group and bring trust to the ML solution, which will improve its chances of success.

As I already said, having good skills in software development is essential to properly code all the steps of the projects. Il allows to deliver faster, more performant, and more maintainable code. In the same category, it is highly beneficial to be able to have deployment skills to at least understand the constraints a deployed solution has and take it into account even if you are not responsible for the deployment itself.

### What is the differences between a data scientist and a data analyst?

I think today there are more and more job titles because the data domain is getting more and more mature. What a single data scientist used to do alone in the past become a set of different topics where people can have a specific expertise (deployment for example). However, not many companies have a clear understanding of the subtilties between job titles and use them randomly. The term data scientist is sexier than data analyst even if the person is supposed to just creates monthly sales metric. It is then important to understand what the actual tasks would be within the company during the interview.

In my mind, a data analyst only makes the exploratory part of a data science project. He/she is responsible to take a dataset and try to identify patterns inside the data that can be leveraged by the business. A data analyst may very well use machine learning algorithms for this purpose. A would say that a data scientist will also be working with predictive models.

### In your opinion, what is/are the best language(s) to learn for a data scientist/analyst? Python, Matlab, java, c++...

There is no best language for data science. It mostly depends on the problem at hand. Python is currently the most popular language because it is easy to code, allows interactivity with jupyter notebooks and has libraries coded in C++ which partially compensate its slowness. C++ is a language which is extremely fast so may be more adapted to high intensity applications. There are even languages dedicated to IOT.

### How do you see the data sciences in 5 years?
The data science domain is evolving quite fast those lasts few years. First, it is gaining in maturity in organizations. This means that there will be more and more dedicated roles within the data community, just like IT has now many different roles de create and maintain software. In the machine learning community, we already start to see people specializing in a single technique because the topic has become too vast for one person to be really an expert in more than a couple of techniques.

Because of the maturity of the domain, we start to have big discussions about security and ethics of the domain. We see models that discriminate against populations, models that are specifically created to discriminate populations, models that are applied to highly sensitive areas (health, defense, …). Just like websites had their GDPR in Europe to limit the information they can freely collect about citizens, IA will definitely have its regulations to define what companies can and can’t do with those systems. This will have an impact on the technologies as well as those system will likely be required to explain why they took a specific decision. The whole field of explainable AI if growing at rapid pace.

### Do you have Mooc (edx, cousera…) or books to recommend me?

I learned most of my current knowledge through online resources. Here is a list of the ones which have taught me the most.

*	**Data analysis**
I had the opportunity to teach data analysis to masters students in 2021. This course condensed what I have learned in my early career in research and techniques that I discovered later on. I have not read a course with the same objective in mind so I will recommend this one on my gitlab repo: https://gitlab.com/cgoudet/esilv. This course also has a chapter dedicated to best practices in several topics.

*	**Machine learning**
Most courses present online only show the application of a simple technique on a simple dataset, without considering how an actual project would happen. This course from fastai is a good introduction to machine learning in practice: Introduction to machine learning for coders


*	**Deployment**
Being able to deploy a ML solution in production is one of the most underrated skill by most formations. A ML solution need to be properly deployed and tracked to ensure its performances during its life cycle. The following book is a reference on everything around the algorithm itself to create IA powered application : Building Machine Learning Powered Applications

* **Deep learning**
Deep learning is one of the hot topics of the moment. Many platforms propose paid courses on the matter. The fastai organization proposes a very qualitative free course on the topic, used a high-level API : Deep Learning for Coders
