# Data scientist' skills

Most academic formations focus on the technical aspect of machine learning.
Students are taught the various algorithms and data preprocessing.
However, this only represent a small fraction of what is actually needed in a machine learning project in a company.
Usually, the juniors lack the remaining set of skills, which stops them from being able to manage a machine learning project from start to end.

This discussion aims at identifying those skills and providing ressources for students to acquire them.
Most of these skills have been identified through multiple internship interviews and discussions with recruiters.

Many topics discussed here have a more in-depth description in [Building Machine Learning Powered Applications](https://www.oreilly.com/library/view/building-machine-learning/9781492045106/) by Emmanuel Ameisen.
This reading is highly recommended.

## SQL

Most datasets in kaggle or in courses come in the form of one or multiple CSV files.
On the other side we often hear about big data and the associated technologies.
However most companies lie in between those two extreme situations: they store their data in SQL database.
As a result, data scientists must often deal and interact with those databases.
From the point of view of a recruiter, good skills in SQL is a must have for a junior data scientist, to be able to be directly able to deal with the data.

This [video](https://www.youtube.com/watch?v=HXV3zeQKqGY) proposes a complete course on the subject.
However, knowledge of basic `SELECT`, ```UPDATE``` and ```JOIN``` may go a long way (see the technical test of the food startup).

## Understanding of business context
In academia, most applications of machine learning are "Take the dataset X and try go get the best metric value prediction y".
One question that is never asked is : why do we even bother trying to predict y?

In companies, machine learning is one tool among many to solve problems.
What is the most important is the problem itself and how it impacts the company.
Most of the time, the problems have nothing to do with a machine learning metric. They fall around :
- this action takes too much time,
- our clients are not happy,
- we don't know how to anticipate...

The data scientist must understand what are the daily challenges of the end user of the system.
Often people come directly with a solution : "I need this prediction".
However it turns out that creating the prediction they ask will not actually help them and the project will be considered a failure.
One must then totally understand the client to identify the root cause of their problem and solve this instead of the symptoms.

## Storytelling

In your daily activities, you will be confronted to non expert people.
This can be the project manager, clients, colleagues...
You will need many people for your projects to be useful.
Hence you must be able to tell them about your project.
You need to make them understand what you do, why you do it, what value it will bring to them or to users.
If people understand you, they will be more likely to contribute positively to your project.
The last section of this [course on good practices](https://gitlab.com/cgoudet/esilv/-/blob/master/notebooks/report/_good_practices.ipynb) proposes tips and ressources to improve this skill.


## Solution design

When you understood what problem you want to solve, you must decide how you will solve it.
Usually you will think about what model to use.
However this is only a small part of the problem.
You must ask the question : how your model will interact with the environment, users and data.
You model is not the solution.
It is only a sub part.
This [article](https://www.oreilly.com/radar/drivetrain-approach-data-products/) is a must read to understand how to think about the global solution and not just the machine learning part.

Identify first the simplest solution that can solve the problem.
This most likely does not involve machine learning.
Machine learning solutions are long and expensive to create.
Even though it is sexy, start first by creating the most simple solution possible.
If your users like how you propose the solution but need more performances, you can then go to more expensive solutions.

In order to know if your users like your solution, you must have a way to measure it.
When designing your solution, think in advance how to measure the performances of your solution, and identify acceptable levels for those performances.

Do not design your solution to be valuable only completed.
Organise the structure of your project to have independent small bits that you can ship independently and quickly.
This will increase the rythme at which you will get feedback from the users, in order to spot problems as soon as possible.

## Software engineering

A machine learning model is only useful if actually used by its intended users.
There are many steps from a working model in a notebook to a production ready solution.

### Pipeline industrialisation

The standard pipeline starts by reading a CSV, then creating features and then training the model.
However in practice, the user may not provide data through a CSV file or in exactly the same format.
It is then necessary to create a pipeline that take the data to the model by creating features with exactly the same definition as for training.

### Code quality
The daily tasks of the data scientist involve a lot of code.
This means that the data scientist is also subject to all risks of software engineering : technical debt, instabilities....
It is then essential to use the best practices of code development to ensure a long term value of the project.
I proposed in another course an introduction to [software development best practices](https://gitlab.com/cgoudet/esilv/-/blob/master/notebooks/report/_good_practices.ipynb).
These rules are meant to help multiple people to collaborate effectively on a project.

### Shipping to production
Models need to be available to users.
Users wont load a jupyter notebook to get their predictions.
The data scientist must think ahead of time how the data will be preprocessed, how the model will be loaded and how the predictions will be used, to define the required infrastructure.

One standard approach is to deploy the model in a web service.
The users will be able to make a web request to get information.
[This article](https://python.plainenglish.io/how-to-use-python-and-fastapi-to-deploy-machine-learning-models-on-heroku-61b96271d5b3) how to deploy a model through a free web service platform.

## Interviews

Not many formations provide tips on how to go to a data scientist job interviews.
Many ressources exist about technical questions but very few on how to deal with non expert recruiters.
Here are a couples of tips to help prepare an interview.

### Understand the company

A company is hiring because they have a need.
They are looking for someone to solve one or many of their problems.
If you better understand the company's issues, you will be able to adapt your speech to what they look for.
Prepare the interview by looking as much information as possible about the company to understand what it needs.

### Adapt the presentation

Once you understand the issues of the recruiting company, you can adapt your speech to show that you already dealt with exactly their problems.
Do not tell a company that you can solve their problems, prove them that you already have solved exactly those problems in your experience.
This is extremely reassuring for a recruiter.

You most certainly have many projects in your experience.
Students have many academic projects and professionals can add their experiences.
However, they are not as meaningful for a recruiter.
Do not speak about all your projects (they are on linkedin anyway).
Choose the most meaningful project with respect to the company's problems, and discuss in details how you solved the problem.

### Storytelling

Most of the interviews will not be with technical experts.
You will have to talk about your projects a lot to non expert people (in your job too).
Practice telling a story around your project.
What was the problem initially?
How did you chose to solve it?
Why this way?
How well did it work?
Put the recruiter in the shoes of the person having the problem and let them understand how the solution help them.
You will then show the value you brought to your users.
See earlier for an introduction course on the topic.

### Ask questions
When the recruiter start talking about their projects, ask a lot of questions.
This will allow to better understand the issues of the company and it will show that you are interested.
You can also use those questions to show that you understand the problematic, and show how quickly you deal with new topics.
